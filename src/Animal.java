import java.lang.String;

public class Animal {
  String name;

  public Animal(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("Animal [Name = %s]", name);

  }

}
